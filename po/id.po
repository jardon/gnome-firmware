# Indonesian translation for gnome-firmware.
# Copyright (C) 2022 gnome-firmware's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-firmware package.
# Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-firmware master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/gnome-firmware/issues\n"
"POT-Creation-Date: 2022-11-09 08:33+0000\n"
"PO-Revision-Date: 2022-11-16 11:15+0700\n"
"Last-Translator: Kukuh Syafaat <kukuhsyafaat@gnome.org>\n"
"Language-Team: Indonesian <gnome-l10n-id@googlegroups.com>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.2.1\n"

#. TRANSLATORS: the application name
#. window title
#: data/appdata/org.gnome.Firmware.metainfo.xml.in:8
#: data/org.gnome.Firmware.desktop.in:3 src/gfu-main.ui:73
msgid "Firmware"
msgstr "Perangkat Tegar"

#. TRANSLATORS: one-line description for the app
#: data/appdata/org.gnome.Firmware.metainfo.xml.in:10
#: data/org.gnome.Firmware.desktop.in:4 src/gfu-main.c:1421
msgid "Install firmware on devices"
msgstr "Pasang perangkat tegar pada perangkat"

#. TRANSLATORS: AppData description marketing paragraph
#: data/appdata/org.gnome.Firmware.metainfo.xml.in:13
msgid "Update, reinstall and downgrade firmware on devices supported by fwupd."
msgstr ""
"Mutakhirkan, pasang ulang, dan turunkan perangkat tegar pada perangkat yang "
"didukung oleh fwupd."

#: data/appdata/org.gnome.Firmware.metainfo.xml.in:20
msgid "Devices panel"
msgstr "Panel perangkat"

#: data/appdata/org.gnome.Firmware.metainfo.xml.in:24
msgid "Releases panel"
msgstr "Panel rilis"

#: data/appdata/org.gnome.Firmware.metainfo.xml.in:28
msgid "Richard Hughes"
msgstr "Richard Hughes"

#. TRANSLATORS: these are desktop keywords - remember the trailing ';' :)
#: data/org.gnome.Firmware.desktop.in:12
msgid "Firmware;"
msgstr "Perangkat Tegar;"

#. TRANSLATORS: We verified the payload against the server
#: src/gfu-common.c:142
msgid "Trusted payload"
msgstr "Payload tepercaya"

#. TRANSLATORS: We verified the meatdata against the server
#: src/gfu-common.c:146
msgid "Trusted metadata"
msgstr "Metadata tepercaya"

#. TRANSLATORS: version is newer
#: src/gfu-common.c:150
msgid "Is upgrade"
msgstr "Apakah peningkatan"

#. TRANSLATORS: version is older
#: src/gfu-common.c:154
msgid "Is downgrade"
msgstr "Apakah diturunkan"

#. TRANSLATORS: version cannot be installed due to policy
#: src/gfu-common.c:158
msgid "Blocked version"
msgstr "Versi yang diblokir"

#. TRANSLATORS: version cannot be installed due to policy
#: src/gfu-common.c:162
msgid "Not approved"
msgstr "Tidak disetujui"

#. TRANSLATORS: is not the main firmware stream
#: src/gfu-common.c:166
msgid "Alternate branch"
msgstr "Cabang alternatif"

#. TRANSLATORS: is not supported by the vendor
#: src/gfu-common.c:171
msgid "Community supported"
msgstr "Didukung komunitas"

#. TRANSLATORS: Device cannot be removed easily
#: src/gfu-common.c:297
msgid "Internal device"
msgstr "Perangkat internal"

#. TRANSLATORS: Device is updatable in this or any other mode
#: src/gfu-common.c:301
msgid "Updatable"
msgstr "Dapat dimutakhirkan"

#. TRANSLATORS: Update can only be done from offline mode
#: src/gfu-common.c:305
msgid "Update requires a reboot"
msgstr "Pemutakhiran membutuhkan nyala ulang"

#. TRANSLATORS: Must be plugged in to an outlet
#: src/gfu-common.c:309
msgid "System requires external power source"
msgstr "Sistem membutuhkan sumber daya eksternal"

#. TRANSLATORS: Is locked and can be unlocked
#: src/gfu-common.c:313
msgid "Device is locked"
msgstr "Perangkat terkunci"

#. TRANSLATORS: Is found in current metadata
#: src/gfu-common.c:317
msgid "Supported on LVFS"
msgstr "Didukung di LVFS"

#. TRANSLATORS: Requires a bootloader mode to be manually enabled by the user
#: src/gfu-common.c:321
msgid "Requires a bootloader"
msgstr "Membutuhkan bootloader"

#. TRANSLATORS: Requires a reboot to apply firmware or to reload hardware
#: src/gfu-common.c:325
msgid "Needs a reboot after installation"
msgstr "Membutuhkan nyala ulang setelah pemasangan"

#. TRANSLATORS: Requires system shutdown to apply firmware
#: src/gfu-common.c:329
msgid "Needs shutdown after installation"
msgstr "Perlu dimatikan setelah pemasangan"

#. TRANSLATORS: Has been reported to a metadata server
#: src/gfu-common.c:333
msgid "Reported to LVFS"
msgstr "Dilaporkan ke LVFS"

#. TRANSLATORS: User has been notified
#: src/gfu-common.c:337
msgid "User has been notified"
msgstr "Pengguna telah diberitahu"

#. TRANSLATORS: Install composite firmware on the parent before the child
#: src/gfu-common.c:345
msgid "Install to parent device first"
msgstr "Pasang ke perangkat induk terlebih dahulu"

#. TRANSLATORS: Is currently in bootloader mode
#: src/gfu-common.c:349
msgid "Is in bootloader mode"
msgstr "Sedang dalam mode bootloader"

#. TRANSLATORS: The hardware is waiting to be replugged
#: src/gfu-common.c:353
msgid "Hardware is waiting to be replugged"
msgstr "Perangkat keras sedang menunggu untuk ditancapkan kembali"

#. TRANSLATORS: Ignore validation safety checks when flashing this device
#: src/gfu-common.c:357
msgid "Ignore validation safety checks"
msgstr "Abaikan pemeriksaan keamanan validasi"

#. TRANSLATORS: Device update needs to be separately activated
#: src/gfu-common.c:369
msgid "Device update needs activation"
msgstr "Pemutakhiran perangkat membutuhkan aktivasi"

#. TRANSLATORS: Device will not return after update completes
#: src/gfu-common.c:385
msgid "Device will not re-appear after update completes"
msgstr "Perangkat tidak akan muncul kembali setelah pemutakhiran selesai"

#. TRANSLATORS: Device supports some form of checksum verification
#: src/gfu-common.c:389
msgid "Cryptographic hash verification is available"
msgstr "Verifikasi hash kriptografi tersedia"

#. TRANSLATORS: Device supports a safety mechanism for flashing
#: src/gfu-common.c:397
msgid "Device stages updates"
msgstr "Pemutakhiran tahapan perangkat"

#. TRANSLATORS: Device supports a safety mechanism for flashing
#: src/gfu-common.c:401
msgid "Device can recover flash failures"
msgstr "Perangkat dapat memulihkan kegagalan flash"

#. TRANSLATORS: Device remains usable during update
#: src/gfu-common.c:405
msgid "Device is usable for the duration of the update"
msgstr "Perangkat dapat digunakan selama durasi pemutakhiran"

#. TRANSLATORS: Device does not jump directly to the newest
#: src/gfu-common.c:409
msgid "Device installs all intermediate releases"
msgstr "Perangkat memasang semua rilis perantara"

#. TRANSLATORS: Device does not restart and requires a manual action
#: src/gfu-common.c:413
msgid "Device skips restart"
msgstr "Perangkat melewati mulai ulang"

#. TRANSLATORS: the stream can be provided by different vendors
#: src/gfu-common.c:417
msgid "Device supports switching to a different stream of firmware"
msgstr "Perangkat mendukung peralihan ke aliran perangkat tegar yang berbeda"

#. TRANSLATORS: it is saved to the users local disk
#: src/gfu-common.c:421
msgid "Device firmware will be saved before installing updates"
msgstr "Perangkat tegar perangkat akan disimpan sebelum memasang pemutakhiran"

#. TRANSLATORS: on some systems certain devices have to have matching versions,
#. * e.g. the EFI driver for a given network card cannot be different
#: src/gfu-common.c:427
msgid "All devices of the same type will be updated at the same time"
msgstr ""
"Semua perangkat dengan tipe yang sama akan dimutakhirkan pada saat yang sama"

#. TRANSLATORS: some devices can only be updated to a new semver and cannot
#. * be downgraded or reinstalled with the sexisting version
#: src/gfu-common.c:434
msgid "Only version upgrades are allowed"
msgstr "Hanya peningkatan versi yang diizinkan"

#. TRANSLATORS: the device is currently unreachable, perhaps because it is in a
#. * lower power state or is out of wireless range
#: src/gfu-common.c:441
msgid "Device is currently unreachable"
msgstr "Perangkat saat ini tidak dapat dijangkau"

#. TRANSLATORS: a volume with full-disk-encryption was found on this machine,
#. * typically a Windows NTFS partition with BitLocker
#: src/gfu-common.c:448
msgid "May invalidate secrets used to decrypt volumes"
msgstr "Dapat membatalkan rahasia yang digunakan untuk mendekripsi volume"

#. TRANSLATORS: daemon is inactive
#: src/gfu-common.c:545
msgid "Idle…"
msgstr "Menganggur…"

#. TRANSLATORS: decompressing the firmware file
#: src/gfu-common.c:549
msgid "Decompressing…"
msgstr "Mendekompresi…"

#. TRANSLATORS: parsing the firmware information
#: src/gfu-common.c:553
msgid "Loading…"
msgstr "Memuat…"

#. TRANSLATORS: restarting the device to pick up new F/W
#: src/gfu-common.c:557
msgid "Restarting device…"
msgstr "Memulai ulang perangkat…"

#. TRANSLATORS: reading from the flash chips
#: src/gfu-common.c:561
msgid "Reading…"
msgstr "Membaca…"

#. TRANSLATORS: writing to the flash chips
#: src/gfu-common.c:565
msgid "Writing…"
msgstr "Menulis…"

#. TRANSLATORS: erasing contents of the flash chips
#: src/gfu-common.c:569
msgid "Erasing…"
msgstr "Menghapus…"

#. TRANSLATORS: verifying we wrote the firmware correctly
#: src/gfu-common.c:573
msgid "Verifying…"
msgstr "Memverifikasi…"

#. TRANSLATORS: scheduing an update to be done on the next boot
#: src/gfu-common.c:577
msgid "Scheduling…"
msgstr "Menjadwalkan…"

#. TRANSLATORS: downloading from a remote server
#: src/gfu-common.c:581
msgid "Downloading…"
msgstr "Mengunduh…"

#. TRANSLATORS: waiting for user to authenticate
#: src/gfu-common.c:585
msgid "Authenticating…"
msgstr "Mengautentikasi…"

#. TRANSLATORS: waiting for device to do something
#: src/gfu-common.c:589
msgid "Waiting…"
msgstr "Menunggu…"

#. TRANSLATORS: waiting for daemon
#: src/gfu-common.c:593
msgid "Shutting down…"
msgstr "Mematikan…"

#. TRANSLATORS: the attestation failed
#: src/gfu-main.c:249
msgid "Not OK"
msgstr "Tidak OK"

#. TRANSLATORS: the checksum verified
#: src/gfu-main.c:254 src/gfu-main.c:1082
msgid "OK"
msgstr "Oke"

#. TRANSLATORS: as in laptop battery power
#: src/gfu-main.c:266
msgid "System power is too low to perform the update"
msgstr "Daya sistem terlalu rendah untuk melakukan pemutakhiran"

#. TRANSLATORS: as in laptop battery power
#: src/gfu-main.c:270
#, c-format
msgid "System power is too low to perform the update (%u%%, requires %u%%)"
msgstr ""
"Daya sistem terlalu rendah untuk melakukan pemutakhiran (%u%%, membutuhkan %u"
"%%)"

#. TRANSLATORS: for example, a Bluetooth mouse that is in powersave mode
#: src/gfu-main.c:276
msgid "Device is unreachable, or out of wireless range"
msgstr "Perangkat tidak dapat dijangkau, atau di luar jangkauan nirkabel"

#. TRANSLATORS: for example the batteries *inside* the Bluetooth mouse
#: src/gfu-main.c:283
#, c-format
msgid "Device battery power is too low"
msgstr "Daya baterai perangkat terlalu rendah"

#. TRANSLATORS: for example the batteries *inside* the Bluetooth mouse
#: src/gfu-main.c:286
#, c-format
msgid "Device battery power is too low (%u%%, requires %u%%)"
msgstr "Daya baterai perangkat terlalu rendah (%u%%, membutuhkan %u%%)"

#. TRANSLATORS: usually this is when we're waiting for a reboot
#: src/gfu-main.c:292
msgid "Device is waiting for the update to be applied"
msgstr "Perangkat sedang menunggu pemutakhiran diterapkan"

#. TRANSLATORS: as in, wired mains power for a laptop
#: src/gfu-main.c:296
msgid "Device requires AC power to be connected"
msgstr "Perangkat membutuhkan daya AC untuk dihubungkan"

#. TRANSLATORS: lid means "laptop top cover"
#: src/gfu-main.c:300
msgid "Device cannot be used while the lid is closed"
msgstr "Perangkat tidak dapat digunakan saat lid tertutup"

#. TRANSLATORS: the hw IDs the device defines,
#. * e.g. USB:0x1234
#: src/gfu-main.c:404
msgid "Vendor ID"
msgid_plural "Vendor IDs"
msgstr[0] "ID Vendor"

#. TRANSLATORS: the checksum state was unknown
#: src/gfu-main.c:434
msgid "Unknown"
msgstr "Tidak diketahui"

#. TRANSLATORS: error in shutting down the system
#: src/gfu-main.c:513
msgid "Failed to shutdown device"
msgstr "Gagal mematikan perangkat"

#: src/gfu-main.c:514
msgid "A manual shutdown is required."
msgstr "Mematikan manual diperlukan."

#. TRANSLATORS: error in rebooting down the system
#: src/gfu-main.c:529
msgid "Failed to reboot device"
msgstr "Gagal menyalakan ulang perangkat"

#: src/gfu-main.c:530
msgid "A manual reboot is required."
msgstr "Diperlukan nyala ulang manual."

#. TRANSLATORS: fwupd refused us data
#: src/gfu-main.c:549
msgid "Failed to load list of remotes"
msgstr "Gagal memuat daftar remote"

#. TRANSLATORS: fwupd refused us data
#: src/gfu-main.c:641
msgid "Failed to load device list"
msgstr "Gagal memuat daftar perangkat"

#. TRANSLATORS: no devices supporting firmware updates were found
#: src/gfu-main.c:670
msgid "No supported devices were found…"
msgstr "Tidak ditemukan perangkat yang didukung…"

#. TRANSLATORS: the list of available firmware failed to be updated
#: src/gfu-main.c:772
msgid "Failed to refresh metadata"
msgstr "Gagal menyegarkan metadata"

#. TRANSLATORS: the LVFS remote was not found on the system
#: src/gfu-main.c:787
msgid "Failed to find LVFS"
msgstr "Gagal menemukan LVFS"

#. TRANSLATORS: the LVFS remote could not be enabled
#: src/gfu-main.c:805
msgid "Failed to enable LVFS"
msgstr "Gagal mengaktifkan LVFS"

#. TRANSLATORS: fwupd refused us data
#: src/gfu-main.c:846
msgid "Failed to get remotes list"
msgstr "Gagal mendapatkan daftar remote"

#. TRANSLATORS: the user needs to do something, e.g. remove the device
#: src/gfu-main.c:883
msgid "To complete the update further action is required"
msgstr "Untuk menyelesaikan pemutakhiran tindakan lebih lanjut diperlukan"

#. TRANSLATORS: the user needs to do something, e.g. remove the device
#: src/gfu-main.c:886
msgid "Action is required"
msgstr "Tindakan diperlukan"

#. TRANSLATORS: the firmware install failed for an unspecified reason
#: src/gfu-main.c:987
msgid "Failed to install firmware"
msgstr "Gagal memasang perangkat tegar"

#. TRANSLATORS: prompting a shutdown to the user
#: src/gfu-main.c:1011
msgid "The update requires the system to shutdown to complete."
msgstr ""
"Pemutakhiran mengharuskan sistem untuk mematikan untuk menyelesaikannya."

#. TRANSLATORS: prompting a reboot to the user
#: src/gfu-main.c:1026
msgid "The update requires a reboot to complete."
msgstr "Pemutakhiran membutuhkan nyala ulang untuk menyelesaikannya."

#. TRANSLATORS: inform the user that the
#. installation was successful onto the device
#: src/gfu-main.c:1042
msgid "Installation successful"
msgstr "Pemasangan berhasil"

#. TRANSLATORS: dialog text, %1 is a
#. * version number, %2 is a device name
#: src/gfu-main.c:1046
#, c-format
msgid "Installed firmware version %s on %s."
msgstr "Versi perangkat tegar yang dipasang %s di %s."

#. TRANSLATORS: e.g. bitlocker
#: src/gfu-main.c:1081
msgid "Full disk encryption detected"
msgstr "Enkripsi diska penuh terdeteksi"

#. TRANSLATORS: buttons
#. TRANSLATORS: button text
#: src/gfu-main.c:1083 src/gfu-main.c:1127 src/gfu-main.c:1188
msgid "Cancel"
msgstr "Batal"

#. TRANSLATORS: the platform secret is stored in the PCRx registers on the TPM
#: src/gfu-main.c:1089
msgid ""
"Some of the platform secrets may be invalidated when updating this firmware."
msgstr ""
"Beberapa rahasia platform mungkin tidak valid saat memutakhirkan perangkat "
"tegar ini."

#. TRANSLATORS: 'recovery key' here refers to a code, rather than a metal thing
#: src/gfu-main.c:1091
msgid "Please ensure you have the volume recovery key before continuing."
msgstr "Pastikan Anda memiliki kunci pemulihan volume sebelum melanjutkan."

#. TRANSLATORS: title, %1 is the firmware vendor, %2 is the
#. device vendor name
#: src/gfu-main.c:1110
#, c-format
msgid "The firmware from %s is not supplied by %s"
msgstr "Perangkat tegar dari %s tidak disediakan oleh %s"

#. TRANSLATORS: branch switch title
#: src/gfu-main.c:1115
msgid "The firmware is not supplied by the vendor"
msgstr "Perangkat tegar tidak disediakan oleh vendor"

#. TRANSLATORS: button text to switch to another stream of firmware
#: src/gfu-main.c:1129
msgid "Switch Branch"
msgstr "Beralih Cabang"

#. TRANSLATORS: %1 is the device vendor name
#: src/gfu-main.c:1134
#, c-format
msgid ""
"Your hardware may be damaged using this firmware, and installing this "
"release may void any warranty with %s."
msgstr ""
"Perangkat keras Anda mungkin rusak menggunakan perangkat tegar ini, dan "
"memasang rilis ini dapat membatalkan garansi apa pun dengan %s."

#. TRANSLATORS: the vendor is unknown
#: src/gfu-main.c:1141
#, c-format
msgid ""
"Your hardware may be damaged using this firmware, and installing this "
"release may void any warranty with the vendor."
msgstr ""
"Perangkat keras Anda mungkin rusak menggunakan perangkat tegar ini, dan "
"memasang rilis ini dapat membatalkan garansi apa pun dengan vendor."

#. TRANSLATORS: should the branch be changed
#: src/gfu-main.c:1148
msgid "You must understand the consequences of changing the firmware branch."
msgstr "Anda harus memahami konsekuensi dari mengubah cabang perangkat tegar."

#. TRANSLATORS: %1 is device name, %2 is the version
#: src/gfu-main.c:1168
#, c-format
msgid "Reinstall %s firmware version %s"
msgstr "Pasang ulang versi perangkat tegar %s %s"

#. TRANSLATORS: %1 is device name, %2 is the version
#: src/gfu-main.c:1171
#, c-format
msgid "Upgrade %s firmware version %s"
msgstr "Peningkatan versi perangkat tegar %s %s"

#. TRANSLATORS: %1 is device name, %2 is the version
#: src/gfu-main.c:1174
#, c-format
msgid "Downgrade %s firmware version %s"
msgstr "Penurunan versi perangkat tegar %s %s"

#. TRANSLATORS: %1 is device name, %2 is the version
#: src/gfu-main.c:1177
#, c-format
msgid "Install %s firmware version %s"
msgstr "Pasang %s versi perangkat tegar %s"

#. TRANSLATORS: button text: install the same version again
#. TRANSLATORS: installing the same firmware that is
#. * currently installed
#: src/gfu-main.c:1193 src/gfu-release-row.c:64
msgid "Reinstall"
msgstr "Pasang ulang"

#. TRANSLATORS: button text, move from old -> new
#. TRANSLATORS: upgrading the firmware
#: src/gfu-main.c:1199 src/gfu-release-row.c:57
msgid "Upgrade"
msgstr "Peningkatan"

#. TRANSLATORS: button text, move from new -> old
#. TRANSLATORS: downgrading the firmware
#: src/gfu-main.c:1203 src/gfu-release-row.c:60
msgid "Downgrade"
msgstr "Penurunan"

#. TRANSLATORS: the device can be used normallly
#: src/gfu-main.c:1210
msgid "The device will remain usable for the duration of the update"
msgstr "Perangkat akan tetap dapat digunakan selama pemutakhiran"

#. TRANSLATORS: the device will be non-fuctional for a while
#: src/gfu-main.c:1215
msgid "The device will be unusable while the update is installing"
msgstr "Perangkat tidak akan dapat digunakan saat pemutakhiran sedang dipasang"

#. TRANSLATORS: verify means checking the actual checksum of the firmware
#: src/gfu-main.c:1290
msgid "Failed to verify firmware"
msgstr "Gagal memverifikasi perangkat tegar"

#. TRANSLATORS: inform the user that the
#. firmware verification was successful
#: src/gfu-main.c:1301
msgid "Verification succeeded"
msgstr "Verifikasi berhasil"

#. TRANSLATORS: firmware is cryptographically identical
#: src/gfu-main.c:1305
#, c-format
msgid "%s firmware checksums matched"
msgstr "%s checksum perangkat tegar cocok"

#. TRANSLATORS: dialog title
#: src/gfu-main.c:1337
msgid "Verify firmware checksums?"
msgstr "Verifikasi checksum perangkat tegar?"

#. TRANSLATORS: device will "go away" and then "come back"
#: src/gfu-main.c:1341
msgid "The device may be unusable during this action"
msgstr "Perangkat mungkin tidak dapat digunakan selama tindakan ini"

#. TRANSLATORS: verify means checking the actual checksum of the firmware
#: src/gfu-main.c:1354
msgid "Failed to update checksums"
msgstr "Gagal memutakhirkan checksum"

#. TRANSLATORS: dialog title
#: src/gfu-main.c:1385
msgid "Update cryptographic hash"
msgstr "Memperbarui hash kriptografi"

#. TRANSLATORS: save what we have now as "valid"
#: src/gfu-main.c:1389
msgid "Record current device cryptographic hashes as verified?"
msgstr "Merekam hash kriptografi perangkat saat ini sebagai terverifikasi?"

#. TRANSLATORS: the title of the about window
#: src/gfu-main.c:1413
msgid "About GNOME Firmware"
msgstr "Tentang GNOME Perangkat Tegar"

#. TRANSLATORS: the application name for the about UI
#. TRANSLATORS: command description
#: src/gfu-main.c:1416 src/gfu-main.c:1920
msgid "GNOME Firmware"
msgstr "GNOME Perangkat Tegar"

#. TRANSLATORS: you can put your name here :)
#: src/gfu-main.c:1430
msgid "translator-credits"
msgstr "Kukuh Syafaat <kukuhsyafaat@gnome.org>, 2022."

#: src/gfu-main.c:1477
msgid "Category"
msgid_plural "Categories"
msgstr[0] "Kategori"

#. title: hashes of the file payload
#: src/gfu-main.c:1496 src/gfu-main.ui:672
msgid "Checksum"
msgid_plural "Checksums"
msgstr[0] "Checksum"

#. TRANSLATORS: list of security issues, e.g. CVEs
#: src/gfu-main.c:1514
msgid "Fixed Issue"
msgid_plural "Fixed Issues"
msgstr[0] "Masalah yang Diperbaiki"

#. TRANSLATORS: as in non-free
#: src/gfu-main.c:1552
msgid "Proprietary"
msgstr "Proprietari"

#. TRANSLATORS: maybe try Linux?
#: src/gfu-main.c:1643 src/gfu-main.c:1754
msgid "The fwupd service is not available for your OS."
msgstr "Layanan fwupd tidak tersedia untuk SO Anda."

#. TRANSLATORS: unlock means to make the device functional in another mode
#: src/gfu-main.c:1667
msgid "Failed to unlock device"
msgstr "Gagal membuka kunci perangkat"

#. TRANSLATORS: prompting a shutdown to the user
#: src/gfu-main.c:1681
msgid "Unlocking a device requires the system to shutdown to complete."
msgstr ""
"Membuka kunci perangkat mengharuskan sistem untuk mematikan untuk "
"menyelesaikannya."

#. TRANSLATORS: prompting a reboot to the user
#: src/gfu-main.c:1697
msgid "Unlocking a device requires a reboot to complete."
msgstr ""
"Membuka kunci perangkat membutuhkan nyala ulang untuk menyelesaikannya."

#. TRANSLATORS: button text
#: src/gfu-main.c:1700
msgid "Restart now?"
msgstr "Mulai ulang sekarang?"

#. TRANSLATORS: command line option
#: src/gfu-main.c:1909
msgid "Show extra debugging information"
msgstr "Tampilkan informasi pengawakutuan tambahan"

#. TRANSLATORS: the user has sausages for fingers
#: src/gfu-main.c:1924
msgid "Failed to parse command line options"
msgstr "Gagal mengurai opsi baris perintah"

#. window title
#: src/gfu-main.ui:19
msgid "Devices"
msgstr "Perangkat"

#. LVFS is an online firmware provider
#: src/gfu-main.ui:98
msgid "LVFS is not enabled"
msgstr "LVFS tidak diaktifkan"

#. we need metadata so we know where to get the firmware from
#: src/gfu-main.ui:108
msgid ""
"Firmware metadata can be obtained from the Linux Vendor Firmware Service at "
"no cost."
msgstr ""
"Metadata perangat tegar dapat diperoleh dari Layanan Perangat Tegar Vendor "
"Linux tanpa biaya."

#. button text
#: src/gfu-main.ui:119
msgid "Enable"
msgstr "Aktifkan"

#. group title
#: src/gfu-main.ui:150
msgid "Device Properties"
msgstr "Properti Perangkat"

#. title: device version
#: src/gfu-main.ui:155
msgid "Current Version"
msgstr "Versi Saat Ini"

#. title: if the device is locked
#: src/gfu-main.ui:165
msgid "Lock Status"
msgstr "Status Kunci"

#. title: the lowest version we can update to
#: src/gfu-main.ui:176
msgid "Minimum Version"
msgstr "Versi Minimum"

#. title: the version of the [read only] bootloader
#: src/gfu-main.ui:186
msgid "Bootloader Version"
msgstr "Versi Bootloader"

#. title: device manufacturer
#. title: manufacturer that supplied the file
#: src/gfu-main.ui:196 src/gfu-main.ui:530
msgid "Vendor"
msgstr "Vendor"

#. title: firmware stream, e.g. coreboot
#: src/gfu-main.ui:206
msgid "Branch"
msgstr "Cabang"

#. title: ime in seconds to update
#. title: time to deploy this specific update
#: src/gfu-main.ui:226 src/gfu-main.ui:632
msgid "Install Duration"
msgstr "Durasi Pemasangan"

#. title: how many times the device can be updated
#: src/gfu-main.ui:236
msgid "Flashes Left"
msgstr "Berkedip Ke Kiri"

#. title: the error text from las time
#: src/gfu-main.ui:246
msgid "Update Error"
msgstr "Kesalahan Pemutakhiran"

#. title: problems why we can't use the device
#: src/gfu-main.ui:262
msgid "Problems"
msgstr "Masalah"

#. title: device unique ID
#: src/gfu-main.ui:278
msgid "Serial Number"
msgstr "Nomor Seri"

#. title: if we can verify the firmware
#: src/gfu-main.ui:288
msgid "Attestation"
msgstr "Pengesahan"

#. button: save as trusted
#: src/gfu-main.ui:305
msgid "_Store"
msgstr "_Simpan"

#. button: verify the checksum
#: src/gfu-main.ui:319
msgid "_Verify"
msgstr "_Verifikasi"

#. title: there is firmware we can install
#: src/gfu-main.ui:361
msgid "Available Releases"
msgstr "Rilis yang Tersedia"

#. title: there are no releases to install
#: src/gfu-main.ui:377
msgid "No Releases Available"
msgstr "Tidak Ada Rilis yang Tersedia"

#. title: when starting
#: src/gfu-main.ui:390
msgid "Loading"
msgstr "Memuat"

#. more information about when we're loading
#: src/gfu-main.ui:406
msgid "Loading devices…"
msgstr "Memuat perangkat…"

#. title: no devices could be found we can update
#: src/gfu-main.ui:422
msgid "No Devices"
msgstr "Tidak Ada Perangkat"

#. title: one line
#: src/gfu-main.ui:497
msgid "Summary"
msgstr "Ringkasan"

#. title: multiple lines of prose
#: src/gfu-main.ui:512
msgid "Description"
msgstr "Deskripsi"

#. title: what we are downloading
#: src/gfu-main.ui:540
msgid "Filename"
msgstr "Nama berkas"

#. title: size in bytes
#: src/gfu-main.ui:558
msgid "Size"
msgstr "Ukuran"

#. title: method of doing the update, e.g. DFU
#: src/gfu-main.ui:568
msgid "Protocol"
msgstr "Protokol"

#. title: the nice name of the firmware remote
#: src/gfu-main.ui:581
msgid "Remote ID"
msgstr "ID Remote"

#. title: the ID to identify the stream
#: src/gfu-main.ui:594
msgid "AppStream ID"
msgstr "ID AppStream"

#. title, e.g. Nonfree, GPLv2+ etc
#: src/gfu-main.ui:612
msgid "License"
msgstr "Lisensi"

#. title: Information about the release, e.g. is-signed
#: src/gfu-main.ui:622
msgid "Flags"
msgstr "Tanda"

#. title: any message we show the user after the update is done
#: src/gfu-main.ui:642
msgid "Update Message"
msgstr "Pesan Pemutakhiran"

#. title: the type of firmware, e.g. X-ManagementEngine
#: src/gfu-main.ui:652
msgid "Categories"
msgstr "Kategori"

#. title: list of security issues, e.g. CVE numbers
#: src/gfu-main.ui:662
msgid "Issues"
msgstr "Masalah"

#. menu-text: download a new copy of the firmware metadata
#: src/gfu-main.ui:703
msgid "_Refresh Metadata"
msgstr "Sega_rkan Metadata"

#. menu-text: select and install a local file
#: src/gfu-main.ui:707
msgid "_Install Firmware Archive"
msgstr "Pasang Ars_ip Perangkat Tegar"

#. menu-text: shows information about this ap
#: src/gfu-main.ui:711
msgid "_About"
msgstr "Tent_ang"

#. TRANSLATORS: problems are things the user has to fix, e.g. low battery
#: src/gfu-release-row.c:75
msgid "Cannot perform action as the device has a problem"
msgstr "Tidak dapat melakukan tindakan karena perangkat mengalami masalah"

#~ msgid "Plugin Defined"
#~ msgstr "Pengaya Didefinisikan"

#~ msgid "GUID"
#~ msgid_plural "GUIDs"
#~ msgstr[0] "GUID"

#~ msgid "Less than one minute remaining"
#~ msgstr "Kurang dari satu menit tersisa"

#~ msgid "%.0f minute remaining"
#~ msgid_plural "%.0f minutes remaining"
#~ msgstr[0] "%.0f menit tersisa"
